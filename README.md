# Weather app


## Description
Welcome to my weather app! This is a weather application built with Django, React, Redux, and Tailwind CSS. Insipired by my parents who own a farm. They constantly rely on weather applications and thus, I wanted to make a weather application thats simple to use yet provided the necessary information. I implemented the Open Weather API into my backend to get all of the weather data.


## Usage
When the application first loads users will be introduced to this landing page. Once they click on the get started button they will be directed to the main page.

![Landing page](./images/Landingpage.png)

Once on the main page users can see all of their previously saved locations with the current weather description and temperature. If the user wants to search for a location all they have to do is type in the city and state in the inputs on the nav bar. The location will then dynamically load inside a card with the associated information. If the user wishes to delete a location they can click on the delete button. If they wish to see the six day forecast they can click on the show more button which will direct them to next page.

![Main page](./images/Savedlocations.png)

Once they show more button is clicked the user is directe to this page. The six day forecast is shown with the average max/min temp and percipitation forecast. Note, the styling and layout on this page is still a work in progress.

![Forecast page](./images/Detailpage.png)

## Project Initialization
To enjoy this project on your local machine follow these steps:
1. Clone this repository.
2. CD into the new project.
3. Run docker-compose build
4. Run docker-compose up
5. Enjoy!
