from django.contrib import admin
from .models import Locations
# Register your models here.
class LocationsAdmin(admin.ModelAdmin):
    list_display = ('city', 'state')

# Register your models here.

admin.site.register(Locations, LocationsAdmin)
