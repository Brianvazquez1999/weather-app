from django.urls import path
from .views import get_locations, location_details

urlpatterns = [
    path("locations/", get_locations, name="get_locations" ),
    path("<int:id>/locations/", location_details, name="location_details")
]
