
import json
import requests
from .keys import KEY
import statistics
from datetime import date, datetime

def get_current_weather(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "appid": KEY
    }

    url = "http://api.openweathermap.org/geo/1.0/direct?"

    response = requests.get(url, params=params)
    content = response.json()
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    url = "https://api.openweathermap.org/data/2.5/weather?"

    current_weather_params = {
        "lat":latitude,
        "lon": longitude,
        "appid": KEY,
        "units": "imperial"
    }

    response = requests.get(url, params=current_weather_params)
    content = response.json()
    try:
        description = content["weather"][0]["description"]
        temp = content["main"]["temp"]
        humidity = content["main"]["humidity"]
        max_temp = content["main"]["temp_max"]
        min_temp = content["main"]["temp_min"]
        feels_ike = content["main"]["feels_like"]
        wind_speed = content["wind"]["speed"]
    except(KeyError, IndexError):
        return None

    return {"description": description, "temp": temp,"humidity":humidity, "max_temp": max_temp, "min_temp": min_temp, "feels_like": feels_ike, "wind_speed": wind_speed}

def get_weather_forecast(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "appid": KEY
    }

    url = "http://api.openweathermap.org/geo/1.0/direct?"

    response = requests.get(url, params=params)
    content = response.json()
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    url = "http://api.openweathermap.org/data/2.5/forecast?"

    get_weather_params = {
        "lat":latitude,
        "lon": longitude,
        "appid": KEY,
        "units": "imperial"
    }

    response = requests.get(url, params=get_weather_params)
    content = response.json()

    try:
        forecast_dict = {}
        date_value = datetime.strptime(content['list'][0]["dt_txt"], "%Y-%m-%d %H:00:00")
        max_temp_list =[]
        min_temp_list =[]
        percipitation_list = []
        for item in content['list']:
            item_date = datetime.strptime(item["dt_txt"], "%Y-%m-%d %H:00:00")
            date = item_date.date().strftime("%m/%d/%Y")
            if date_value.date() == item_date.date():
                max_temp = item['main']['temp_max']
                max_temp_list.append(max_temp)
                min_temp = item['main']['temp_min']
                min_temp_list.append(min_temp)
                percipitation = item['pop']
                percipitation_list.append(percipitation)
            if item == content['list'][len(content['list']) - 1]:
                date = date_value.date().strftime("%m/%d/%Y")
                forecast_dict[date] = [round(statistics.mean(max_temp_list), 2), round(statistics.mean(min_temp_list),2), round(statistics.mean(percipitation_list),2)]
                date_value = datetime.strptime(item["dt_txt"], "%Y-%m-%d %H:00:00")
                max_temp_list =[]
                min_temp_list =[]
                percipitation_list = []

            elif date_value.date() != item_date.date() or content['list'][len(content['list']) - 1] == item:
                date = date_value.date().strftime("%m/%d/%Y")
                forecast_dict[date] = [round(statistics.mean(max_temp_list), 2), round(statistics.mean(min_temp_list),2), round(statistics.mean(percipitation_list),2) * 100]
                date_value = datetime.strptime(item["dt_txt"], "%Y-%m-%d %H:00:00")
                max_temp_list =[]
                min_temp_list =[]
                percipitation_list = []





    except(KeyError, IndexError):
        return None
    return {"forecast": forecast_dict}
