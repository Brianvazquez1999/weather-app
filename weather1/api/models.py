from django.db import models

# Create your models here.
class Locations(models.Model) :
    city = models.CharField(max_length=240)
    state = models.CharField(max_length=2)
