from django.shortcuts import render
from .models import Locations
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.core.exceptions import ValidationError
from .acl import get_current_weather, get_weather_forecast


@require_http_methods(["GET", "POST"])
def get_locations(requests) :
    if requests.method == 'GET' :
        location_list = []
        locations = Locations.objects.all()
        for location in locations:
            current_weather = get_current_weather(location.city, location.state)
            weather_forecast = get_weather_forecast(location.city, location.state)
            location_list.append({
                "city": location.city,
                "state": location.state,
                "weather": current_weather,
                "forecast": weather_forecast,
                "id": location.id,
            })
        return JsonResponse({"location": location_list})
    else:
        try:
            content = json.loads(requests.body)
            location = Locations.objects.create(**content)
        except ValidationError:
            return JsonResponse({"message": 'Invalid fields'})
        return JsonResponse({"city": location.city, "state": location.state})
@require_http_methods(["DELETE" ,"GET"])
def location_details(requests, id):
    if requests.method == 'DELETE':
        count, _ = Locations.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        location = Locations.objects.get(id=id)
        current_weather = get_current_weather(location.city, location.state)
        weather_forecast = get_weather_forecast(location.city, location.state)
        return JsonResponse({"city":location.city, "state":location.state, "id": location.id, "weather": current_weather, "forecast": weather_forecast})
