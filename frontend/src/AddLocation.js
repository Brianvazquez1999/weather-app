import {
  handleCityChange,
  handleStateChange,
  reset,
} from "./app/newLocationSlice";
import { useSelector, useDispatch } from "react-redux";
import { useAddLocationMutation } from "./app/locationsApi";
import {
  Typography,
  Button,
  IconButton,
  Input,
} from "@material-tailwind/react";

function AddLocation() {
  const dispatch = useDispatch();
  const [addLocation] = useAddLocationMutation();
  const newLocation = useSelector((state) => state.newLocation);

  return (
    <>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          addLocation(newLocation);
          dispatch(reset());
        }}
        className="flex flex-col md:flex-row items-stretch"
      >
        <div className="flex-1">
          <Input
            onChange={(e) => {
              dispatch(handleCityChange(e.target.value));
            }}
            value={newLocation.city}
            name="city"
            className="form-control text-white"
            variant="outlined"
            label={<label className="text-white">City</label>}
            type="search"
            aria-label="Search"
          />
        </div>

        <div className="flex-1">
          <Input
            onChange={(e) => {
              dispatch(handleStateChange(e.target.value));
            }}
            value={newLocation.state}
            name="state"
            className="form-control"
            variant="outlined"
            label={<label className="text-white">State</label>}
            type="search"
            aria-label="Search"
          />
        </div>

        <Button type="submit" className="btn btn-success">
          Search
        </Button>
      </form>
    </>
  );
}
export default AddLocation;
