import { useGetLocationByIdQuery } from "./app/locationsApi";
import { Link, useParams } from "react-router-dom";
import { Card, CardBody, IconButton } from "@material-tailwind/react";
import { useState } from "react";
import './index.css'
import {Navbar} from "@material-tailwind/react";
import { Puff } from "react-loader-spinner";
function LocationDetails() {
    const [showForecast, setShowForecast] = useState(false)
    const{id} = useParams()
    const {data, error, isLoading} = useGetLocationByIdQuery(id)
    const today = new Date()
    const formatted = today.toLocaleDateString('en-US', {year: 'numeric', month: 'long', day: 'numeric'})
    let dates =""
    if (data) {
       dates = Object.keys(data.forecast.forecast)
    }

    const handleForecast = () => {
        setShowForecast(!showForecast)

    }
    if (isLoading) {
      return (
        <Puff
          height="600"
          width="80"
          radius={1}
          color="blue"
          ariaLabel="puff-loading"
          wrapperStyle={{
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: "center",
            justifyContent: "center",
          }}
          wrapperClass=""
          visible={true}
        />
      );
    }
    else
    {return (
    <>
    <Navbar className="bg-blue-gray-50 h-max max-w-full flex justify-center">
      <Link  className=" text-black" to={"/locations"}>Home</Link>
    </Navbar>


        <div className="flex justify-center items-center h-screen bg-gradient-to-r from-blue-gray-200 to to-blue-gray-700">


           <Card className="shadow-xl">
      <CardBody>
        <h5 className="font-bold">{data.city}, {data.state}</h5>
        <p className="text-gray-600">{formatted}</p>
        <div className="flex items-center">
          <img
            className="h-24 w-24 mr-4"
            src="https://www.weatherbit.io/static/img/icons/t01d.png"
            alt="weather-icon"
          />
          <div className="flex flex-col items-start">
            <p className="text-2xl font-bold">{data.weather.temp}°F</p>
            <p className="text-gray-600">{data.weather.description}</p>
            <p className="text-gray-600">{data.weather.humidity}% humidity</p>
          </div>
        </div>

          <div className="mt-4">
              <p className="font-medium">Five Day Forecast:</p>
              <ul>

              {dates.map((date) => {
                const dateObject = new Date(date)
                return (

                    <li>{dateObject.toLocaleDateString("en-us",{weekday: 'long'} )}: {data.forecast.forecast[date][0]}°/ {data.forecast.forecast[date][1]}°   &#x2614; percipitation:{data.forecast.forecast[date][2]}%</li>



                )
              })}
              </ul>


            </div>
      </CardBody>
    </Card>
        </div>

</>)}
}
 export default LocationDetails
