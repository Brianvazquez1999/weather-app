import logo from './logo.svg';
import MainPage from './MainPage';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import LocationList from './LocationList';
import Nav from './Nav';
import LocationDetails from './LocationDetails';

function App() {
  return (
  <BrowserRouter>

    <Routes>
      <Route path="/" element={<MainPage/>} />
      <Route path="/locations" element={<LocationList />} />
      <Route path=":id" element={<LocationDetails/> }/>
    </Routes>

  </BrowserRouter>

  );
}

export default App;
