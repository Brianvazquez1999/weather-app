import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
export const locationsApi = createApi({
    reducerPath: 'locations',
    baseQuery: fetchBaseQuery({ baseUrl: "http://localhost:8000/api/" }),
    endpoints: (builder) => ({
      getLocations: builder.query({
        query: () => "locations/",
        providesTags: (result) => {
          const tags = [{type: 'Locations', id: 'LIST'}]
          if (!result) return tags;
          console.log(result)
          return [
            ...result.location.map(({id}) => ({type: 'Locations', id})),
            ...tags
          ]
        }
      }),

      getLocationById: builder.query({
        query:(id) => `${id}/locations/`
      }),

      addLocation: builder.mutation({
        query: (body) => ({
          url: "locations/",
          method: 'POST',
          body
        }),
        invalidatesTags: ['Locations'],
      }),


    deleteLocation: builder.mutation({
      query: (id) =>({
        url: `${id}/locations/`,
        method: 'DELETE',
      }),
      invalidatesTags: (result, error, {id}) => [{type: 'Locations', id}]
    })

    }),


  })

  export const {useGetLocationsQuery, useAddLocationMutation, useDeleteLocationMutation, useGetLocationByIdQuery} = locationsApi;
