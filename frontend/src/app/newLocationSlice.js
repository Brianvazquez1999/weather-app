import { createSlice } from "@reduxjs/toolkit"
const initialState = {
    city: "",
    state: ""
}
export const newLocationSlice = createSlice({
    name: "newLocation",
    initialState,
    reducers: {
        handleCityChange: (state, action) => {
            state.city = action.payload
        },
        handleStateChange: (state, action) => {
            state.state = action.payload
        },
        reset: () => initialState
    },
})

export const {handleCityChange, handleStateChange, reset } = newLocationSlice.actions;
export default newLocationSlice.reducer;
