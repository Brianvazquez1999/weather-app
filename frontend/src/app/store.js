import { configureStore } from '@reduxjs/toolkit'
import { locationsApi } from './locationsApi'
import { setupListeners } from '@reduxjs/toolkit/dist/query'
import newLocationReducer from './newLocationSlice'


export const store =  configureStore({
  reducer: {
    newLocation: newLocationReducer,
    [locationsApi.reducerPath]: locationsApi.reducer
  },

  middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware().concat(locationsApi.middleware),
})


setupListeners(store.dispatch)
