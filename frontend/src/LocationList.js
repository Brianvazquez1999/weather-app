import { useState, useEffect } from "react";
import { Puff } from "react-loader-spinner";
import { Link } from "react-router-dom";
import {
  useGetLocationsQuery,
  useDeleteLocationMutation,
} from "./app/locationsApi";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Button,
} from "@material-tailwind/react";
import Nav from "./Nav";

function LocationList() {
  const [showDetail, setDetail] = useState(false);
  const { data, error, isLoading } = useGetLocationsQuery();
  const [deleteLocation] = useDeleteLocationMutation();

  if (isLoading === false) {
    if (data.location.length === 0) {
      return (
        <>
          <Nav />
          <div className="flex justify-center items-center h-screen">
            <Typography variant="h1" className="text-center">
              Add a location by using the search bar above.
            </Typography>
          </div>
        </>
      );
    }
    return (
      <>
        <Nav />
        <div className="-800 h-screen">
          <Typography variant="h1" className="text-center">
            Saved Locations
          </Typography>
          <div className="grid grid-flow-row gap-8 text-neutral-600 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 py-10 px-12">
            {data.location.map((location) => {
              return (
                <Card className='text-center bg-cover bg-center bg-[url("https://images.unsplash.com/photo-1557683311-eac922347aa1?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1129&q=80")]'>
                  <Typography
                    variant="h5"
                    className="mb-2 font-bold font-roboto"
                    color="white"
                  >
                    {location.city},{location.state}
                    <br></br>
                    <Typography>{location.weather.description}</Typography>
                    <br></br>
                    <Typography>
                      Current Temp: {location.weather.temp}°F
                    </Typography>
                  </Typography>

                  <CardBody>
                    <Link to={`/${location.id}`}>
                      <Button
                        className="btn btn-primary mx-5"
                        type="submit"
                        color="gray"
                      >
                        Show more
                      </Button>
                    </Link>
                    <Button
                      onClick={(e) => deleteLocation(location.id)}
                      type="Button"
                      className="btn btn-danger mx-5"
                      color="red"
                    >
                      Remove
                    </Button>
                  </CardBody>
                </Card>
              );
            })}
          </div>
        </div>
      </>
    );
  } else {
    return (
      <Puff
        height="600"
        width="80"
        radius={1}
        color="blue"
        ariaLabel="puff-loading"
        wrapperStyle={{
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          alignItems: "center",
          justifyContent: "center",
        }}
        wrapperClass=""
        visible={true}
      />
    );
  }
}

export default LocationList;
