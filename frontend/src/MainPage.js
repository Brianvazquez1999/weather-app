import Lottie from "lottie-react";
import weatherAnimation from './lottie/mainpage.json'
import { Link } from "react-router-dom";
function MainPage() {
return (
    <>
        <div className="relative w-screen h-screen flex flex-col justify-center items-center bg-gray-50 overflow-hidden">
        <div className="absolute item-center" >
            <Lottie animationData={weatherAnimation} loop={true} />;
            </div>
            <div className="relative z-10 flex flex-col justify-center items-center h-full">
                <h1 className="py-10 text-3xl sm:text-5xl font-light tracking-wide leading-tight">Welcome to my weather app, <br/>Click below to get started</h1>
                <div className="mt-10 flex sm:flex-row items-center">
                  <Link to={"locations"}> <button className="m-1.5 py-2.5 px-5 rounded-md bg-blue-600 text-white font-semibold uppercase hover:bg-teal-400">Get Started</button></Link>
                </div>
        </div>

        </div>
</>
      )



}

export default MainPage;
